VERSION 5.00
Begin VB.Form Form7 
   BackColor       =   &H00FFFFFF&
   Caption         =   "EDIT ITEMS"
   ClientHeight    =   4650
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   7695
   LinkTopic       =   "Form7"
   ScaleHeight     =   4650
   ScaleWidth      =   7695
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   2040
      TabIndex        =   8
      Top             =   2040
      Width           =   4455
   End
   Begin VB.TextBox Text3 
      Height          =   375
      Left            =   2040
      TabIndex        =   3
      Top             =   2640
      Width           =   4455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "UPDATE"
      Height          =   735
      Left            =   4680
      TabIndex        =   2
      Top             =   3480
      Width           =   1815
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   2040
      TabIndex        =   1
      Text            =   "Select"
      Top             =   1440
      Width           =   4455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "DELETE"
      Height          =   735
      Left            =   2040
      TabIndex        =   0
      Top             =   3480
      Width           =   1935
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Item ID :"
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   1440
      Width           =   615
   End
   Begin VB.Label Label4 
      BackColor       =   &H00C0E0FF&
      BackStyle       =   0  'Transparent
      Caption         =   "Price :"
      Height          =   255
      Left            =   360
      TabIndex        =   6
      Top             =   2760
      Width           =   615
   End
   Begin VB.Label Label2 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Item :"
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   2160
      Width           =   615
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "EDIT ITEMS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2160
      TabIndex        =   4
      Top             =   240
      Width           =   3015
   End
End
Attribute VB_Name = "Form7"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Combo1_Click()
On Error Resume Next

Call Getconnection
rs.Open "select * from items_tbl where ID=" + Combo1.Text, cn, adOpenDynamic, adLockPessimistic
Text3.Text = rs.Fields("price")
Text1.Text = rs.Fields("item")
Call closedb

End Sub

Private Sub Command1_Click()
On Error Resume Next

Call Getconnection
rs.Open "select * from items_tbl where ID=" + Combo1.Text, cn, adOpenDynamic, adLockPessimistic

If rs.EOF = True Then
MsgBox "No Record Found!", vbOKOnly, "Error"
Call closedb
Else
rs.Delete
MsgBox "Item Deleted!", vbOKOnly, "Success"

Text3.Text = ""
Text1.Text = ""

Call bindCombo

End If

End Sub

Private Sub Command2_Click()
On Error Resume Next

Call Getconnection
rs.Open "select * from items_tbl where ID=" + Combo1.Text, cn, adOpenDynamic, adLockPessimistic

If rs.EOF = True Then
MsgBox "Item Not Found!", vbOKOnly, "Error"
Else
rs.Fields("item") = Text1.Text
rs.Fields("price") = Text3.Text
rs.Update
MsgBox "Item Updated!", vbOKOnly, "Success"

Text1.Text = ""
Text3.Text = ""
End If

End Sub

Private Sub Form_Load()

Call bindCombo

End Sub

Function bindCombo()
On Error Resume Next

Combo1.Clear
Combo1.Text = "Select"
Call Getconnection
rs.Open "select * from items_tbl", cn, adOpenDynamic, adLockPessimistic
While rs.EOF = False
Combo1.AddItem (rs.Fields("ID"))
rs.MoveNext
Wend
Call closedb

End Function

