VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmSplash 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4635
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   7380
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmSplash.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4635
   ScaleWidth      =   7380
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H8000000E&
      Height          =   4410
      Left            =   150
      TabIndex        =   0
      Top             =   60
      Width           =   7080
      Begin VB.Timer Timer2 
         Interval        =   25
         Left            =   1080
         Top             =   2400
      End
      Begin ComctlLib.ProgressBar ProgressBar1 
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   3960
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   450
         _Version        =   327682
         Appearance      =   1
      End
      Begin VB.Timer Timer1 
         Interval        =   500
         Left            =   480
         Top             =   2400
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "www.minusbugs.com"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4320
         TabIndex        =   4
         Top             =   3480
         Width           =   2535
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000E&
         BackStyle       =   0  'Transparent
         Caption         =   "Loading"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   3
         Top             =   3360
         Width           =   2535
      End
      Begin VB.Image imgLogo 
         Height          =   705
         Left            =   4560
         Picture         =   "frmSplash.frx":F920
         Stretch         =   -1  'True
         Top             =   2760
         Width           =   2295
      End
      Begin VB.Label lblProductName 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000E&
         Caption         =   "Annapoorna Hotel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   32.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   1200
         TabIndex        =   2
         Top             =   1200
         Width           =   5565
      End
      Begin VB.Label lblCompanyProduct 
         AutoSize        =   -1  'True
         BackColor       =   &H8000000E&
         Caption         =   "Billing Software"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   3960
         TabIndex        =   1
         Top             =   720
         Width           =   2745
      End
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim str, i

Private Sub Timer1_Timer()
On Error Resume Next

If i > 6 Then i = 0

i = i + 1

If i = 1 Then
str = "Loading"
ElseIf i = 2 Then
str = "Loading.."
ElseIf i = 3 Then
str = "Loading...."
ElseIf i = 4 Then
str = "Loading......"
End If

Label1.Caption = str

End Sub

Private Sub Timer2_Timer()


ProgressBar1.Value = ProgressBar1.Value + 1

If ProgressBar1.Value = 100 Then
Timer1.Enabled = False
Timer2.Enabled = False

If FileLen("admin.mdb") = 0 Then
a = MsgBox("Missing files! Please Contact Developers!!", vbCritical, "Error!")
Unload Me
Else
Unload Me
MDIForm1.Show
End If

ElseIf ProgressBar1.Value > 40 And ProgressBar1.Value < 50 Then
Timer2.Interval = 150
ElseIf ProgressBar1.Value = 50 Then
Timer2.Interval = 10
End If


End Sub
