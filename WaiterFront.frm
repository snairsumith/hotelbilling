VERSION 5.00
Begin VB.Form Form10 
   BackColor       =   &H8000000E&
   Caption         =   "Choose Your Role"
   ClientHeight    =   6255
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   5040
   LinkTopic       =   "Form10"
   ScaleHeight     =   6255
   ScaleWidth      =   5040
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "Admin"
      Height          =   975
      Left            =   2520
      TabIndex        =   1
      Top             =   0
      Width           =   2535
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Height          =   975
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   2535
   End
End
Attribute VB_Name = "Form10"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click(Index As Integer)
On Error Resume Next

waiter = Command1(0).Caption
Form12.Show
Unload Me
End Sub

Private Sub Command2_Click()
Unload Me
Form1.Show
End Sub

Private Sub Form_Load()
On Error Resume Next

Dim i As Integer
i = 0

Call Getconnection

rs.Open "select username from login_tbl where role='waiter'", cn, adOpenDynamic, adLockPessimistic

While rs.EOF = False
Load Command1(i)
With Command1(i)
.Caption = rs.Fields(0)
.Top = .Height * i
.Left = 0
.Visible = True
End With
i = i + 1
rs.MoveNext
Wend

Call closedb

End Sub
