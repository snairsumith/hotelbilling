VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form Form12 
   Caption         =   "BILLING"
   ClientHeight    =   6525
   ClientLeft      =   2580
   ClientTop       =   2100
   ClientWidth     =   15270
   LinkTopic       =   "Form12"
   ScaleHeight     =   6525
   ScaleWidth      =   15270
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Height          =   6495
      Left            =   7200
      TabIndex        =   14
      Top             =   0
      Width           =   8055
      Begin VB.CommandButton Command3 
         Caption         =   "Edit"
         Height          =   435
         Left            =   2160
         TabIndex        =   24
         Top             =   960
         Width           =   495
      End
      Begin VB.CommandButton Command2 
         Caption         =   "PRINT"
         Height          =   615
         Left            =   6240
         TabIndex        =   8
         Top             =   5640
         Width           =   1455
      End
      Begin MSDataGridLib.DataGrid DataGrid1 
         Height          =   3975
         Left            =   360
         TabIndex        =   15
         Top             =   1440
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   7011
         _Version        =   393216
         BackColor       =   16777215
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin VB.Label lblTotal 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   555
         Left            =   4440
         TabIndex        =   22
         Top             =   5640
         Width           =   1080
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "TOTAL"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   5760
         Width           =   855
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   6360
         TabIndex        =   20
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Date :"
         Height          =   255
         Left            =   5520
         TabIndex        =   19
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label lblBillNo 
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         Height          =   255
         Left            =   1200
         TabIndex        =   18
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Bill No :"
         Height          =   255
         Left            =   360
         TabIndex        =   17
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "BILL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   3000
         TabIndex        =   16
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   6495
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   7095
      Begin Crystal.CrystalReport lvreport 
         Left            =   840
         Top             =   5760
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   348160
         PrintFileLinesPerPage=   60
      End
      Begin VB.TextBox Text5 
         Height          =   285
         Left            =   2640
         TabIndex        =   0
         Top             =   1320
         Width           =   3495
      End
      Begin VB.ListBox List1 
         Height          =   1425
         ItemData        =   "Billing.frx":0000
         Left            =   2640
         List            =   "Billing.frx":0002
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   2040
         Visible         =   0   'False
         Width           =   3495
      End
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   2640
         TabIndex        =   1
         Top             =   1800
         Width           =   3495
      End
      Begin VB.TextBox Text3 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2640
         TabIndex        =   6
         Top             =   4920
         Width           =   3495
      End
      Begin VB.CommandButton Command1 
         Caption         =   "ADD ITEM"
         Height          =   615
         Left            =   3720
         TabIndex        =   7
         Top             =   5520
         Width           =   2415
      End
      Begin VB.TextBox Text2 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2640
         TabIndex        =   4
         Top             =   4200
         Width           =   3495
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   2640
         TabIndex        =   3
         Top             =   3480
         Width           =   3495
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Waiter ID :"
         Height          =   255
         Left            =   600
         TabIndex        =   23
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Amount :"
         Height          =   255
         Left            =   600
         TabIndex        =   13
         Top             =   4920
         Width           =   855
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Unit Price :"
         Height          =   255
         Left            =   600
         TabIndex        =   12
         Top             =   4200
         Width           =   855
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Quantity :"
         Height          =   255
         Left            =   600
         TabIndex        =   11
         Top             =   3480
         Width           =   855
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Item :"
         Height          =   255
         Left            =   600
         TabIndex        =   10
         Top             =   1920
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "ADD ITEMS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2160
         TabIndex        =   9
         Top             =   360
         Width           =   2775
      End
   End
End
Attribute VB_Name = "Form12"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim today As String
Dim billno

Private Sub Command3_Click()

a = InputBox("Enter New Bill ID:")
lblBillNo.Caption = a

Call bindGrid

End Sub

Private Sub Form_Load()
On Error Resume Next

today = DateTime.Date
Label9.Caption = today

Call getId

'List1.Visible = False


End Sub


Private Sub Command1_Click()
On Error Resume Next

If Text1.Text = "" Or Text2.Text = "" Or Text3.Text = "" Then
MsgBox "Please Fill All Fields!", vbExclamation, "Error"
Else

itm = Text4.Text
q = "select item from items_tbl where ID=" + itm
Call Getconnection
rs.Open q, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
itm = rs.Fields("item")
End If
Call closedb

Call Getconnection
rs.Open "select * from bill_tbl", cn, adOpenDynamic, adLockPessimistic
rs.AddNew
rs.Fields("waiter") = Text5.Text
rs.Fields("item") = itm
rs.Fields("qty") = Text1.Text
rs.Fields("price") = Text2.Text
rs.Fields("total") = Text3.Text
rs.Fields("billid") = Val(lblBillNo.Caption)
rs.Fields("today") = Label9.Caption
rs.Update
Call closedb

Call Getconnection
rs.Open "select sum(total) as total from bill_tbl where billid=" + lblBillNo.Caption, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
Text4.Text = 0
Else
lblTotal.Caption = rs.Fields("total")
rs.Close
End If

Call bindGrid
Text1.Text = ""
Text2.Text = "0"
Text3.Text = "0"
Text4.Text = ""
Text4.SetFocus

End If


End Sub

Private Sub Command2_Click()
'On Error Resume Next

'Load DataEnvironment1

'total = lblTotal.Caption
'bill = lblBillNo.Caption

'With DataEnvironment1
'.Command2 lblBillNo.Caption
'End With
'DataReport1.Show
Call closedb
Call closer

lvreport.Connect = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\admin.mdb;Persist Security Info=False"
lvreport.ReportFileName = App.Path & "\report\bill.rpt"
lvreport.RetrieveDataFiles

p = lvreport.PageCount

lvreport.WindowState = crptMaximized
lvreport.Action = 1



End Sub

Private Sub Frame1_Click()
List1.Visible = False
End Sub

Private Sub Frame2_Click()
List1.Visible = False
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then

Text4.Text = List1.Text
List1.Visible = False
Text1.SetFocus

Call getPrice

End If

End Sub

Private Sub Text1_Change()
On Error Resume Next

Text3.Text = ""
If Text2.Text = "" Then
'MsgBox "Fill all Fields!", vbOKOnly, "Error"
Else
If Text1.Text = "" Then
Else
Text3.Text = Text1.Text * Text2.Text
End If
End If

End Sub

Function getId()
On Error Resume Next

Call Getconnection
rs.Open "select * from bill_tbl", cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
Call closedb
rs.Open "select max(billid) as maxx from bill_tbl", cn, adOpenDynamic, adLockPessimistic
billno = rs.Fields("maxx")
billno = billno + 1
Else
billno = 0
Call closedb
End If

lblBillNo.Caption = billno

End Function


Function bindGrid()
On Error Resume Next

If con.State = 1 Then
con.Close
End If

Call Connect
res.CursorLocation = adUseClient
'res.Open "select * from bill_qry where billid=" + lblBillNo.Caption, con, adOpenDynamic, adLockPessimistic
res.Open "select item as Item,qty as Quantity,price as Price,total as Total from bill_tbl  where billid=" + lblBillNo.Caption, con, adOpenDynamic, adLockPessimistic
Set DataGrid1.DataSource = res

End Function

'Autocomplete n33raj

Private Sub List1_Click()
List1.Visible = True
End Sub

Private Sub List1_DblClick()
Text4.Text = List1.Text
List1.Visible = False
Text1.SetFocus
Call getPrice
End Sub

Private Sub List1_LostFocus()
List1.Visible = False
End Sub

Private Sub Text1_Click()
List1.Visible = False
End Sub

Private Sub Text4_Change()
On Error Resume Next

u = Text4.Text

If Len(u) > 0 Then
q = "select ID from items_tbl where ID like '" + u + "%'"
Call Getconnection
rs.Open q, cn, adOpenDynamic, adLockPessimistic

List1.Clear
While rs.EOF = False
List1.AddItem (rs.Fields("ID"))
rs.MoveNext
Wend

If List1.ListCount > 0 Then
List1.Visible = True
List1.Selected(0) = True
Else
List1.Visible = False
End If
Call closedb

Else
List1.Visible = False
End If

End Sub

Private Sub Text4_LostFocus()
Call getPrice
End Sub

Function getPrice()
On Error Resume Next

Call Getconnection
rs.Open "select * from items_tbl where ID=" + Text4.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
Text2.Text = rs.Fields("price")
End If
Call closedb

End Function

Private Sub Text5_LostFocus()
On Error Resume Next

If Text5.Text <> "" Then
Call Getconnection
rs.Open "select ID from login_tbl where role='waiter' and ID=" + Text5.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
MsgBox "Invalid Waiter ID!"
Text5.Text = ""
Text5.SetFocus
End If
Call closedb
End If

End Sub
