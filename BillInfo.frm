VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Form13 
   Caption         =   "Bill Information"
   ClientHeight    =   7470
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8880
   LinkTopic       =   "Form13"
   ScaleHeight     =   7470
   ScaleWidth      =   8880
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   7695
      Left            =   0
      TabIndex        =   0
      Top             =   -120
      Width           =   8895
      Begin MSDataGridLib.DataGrid DataGrid1 
         Height          =   4215
         Left            =   360
         TabIndex        =   11
         Top             =   3120
         Width           =   8175
         _ExtentX        =   14420
         _ExtentY        =   7435
         _Version        =   393216
         HeadLines       =   1
         RowHeight       =   15
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Delete All"
         Height          =   315
         Left            =   2400
         TabIndex        =   5
         Top             =   2280
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox Text3 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   6840
         PasswordChar    =   "*"
         TabIndex        =   8
         Top             =   1920
         Width           =   1815
      End
      Begin VB.TextBox Text2 
         Enabled         =   0   'False
         Height          =   285
         Left            =   6840
         TabIndex        =   7
         Top             =   1440
         Width           =   1815
      End
      Begin VB.CommandButton Command2 
         Caption         =   "DELETE"
         Height          =   375
         Left            =   7200
         TabIndex        =   6
         Top             =   2280
         Width           =   1455
      End
      Begin VB.CommandButton Command1 
         Caption         =   "SEARCH"
         Height          =   375
         Left            =   1200
         TabIndex        =   4
         Top             =   2280
         Width           =   1095
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         Top             =   1920
         Width           =   1095
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Password :"
         Height          =   255
         Left            =   5880
         TabIndex        =   10
         Top             =   1920
         Width           =   855
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "WaiterID :"
         Height          =   255
         Left            =   5880
         TabIndex        =   9
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Bill No :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   1920
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "BILL INFORMATION"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1920
         TabIndex        =   1
         Top             =   240
         Width           =   4935
      End
   End
End
Attribute VB_Name = "Form13"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
On Error Resume Next

If Text1.Text <> "" Then

If con.State = 1 Then
con.Close
End If

Call Getconnection
rs.Open "select waiter from bill_tbl where billid=" + Text1.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
Text2.Text = rs.Fields(0)
End If
Call closedb

Call Connect
res.CursorLocation = adUseClient
res.Open "select item as Item,qty as Quntity,price as Price,total as Total,ID from bill_tbl where billid=" + Text1.Text, con, adOpenDynamic, adLockPessimistic
If res.EOF = True Then
MsgBox "No Bill Found!", vbOKOnly, "Error"
Else
Set DataGrid1.DataSource = res
End If

Else
MsgBox "Type Bill ID!", vbOKOnly, "Error"
End If


End Sub

Private Sub Command2_Click()
On Error Resume Next

u = Text2.Text
p = Text3.Text

If Text2.Text = "" Or Text3.Text = "" Then
MsgBox "Please Fill All Fields!", vbExclamation, "Error"

Else

Call Getconnection
rs.Open "select * from login_tbl where ID=" + u, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then

If p = rs.Fields("password") Then

Call Getconnection
rs.Open "select * from bill_tbl where billid=" + Text1.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
MsgBox "No Bill Found!", vbOKOnly, "Error"
Else

While rs.EOF = False
rs.Delete
rs.MoveNext
Wend

MsgBox "Bill Details Deleted!", vbOKOnly, "Success"

Text1 = ""
Set DataGrid1.DataSource = res
End If
Call closedb

Else
MsgBox "Invalid password!", vbExclamation, "Error"
End If
Else
MsgBox "Invalid password!", vbExclamation, "Error"
End If

End If

End Sub

Private Sub Command3_Click()
On Error Resume Next

Call Getconnection
rs.Open "select * from bill_tbl where billid=" + Text1.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
MsgBox "No Bill Found!", vbOKOnly, "Error"
Else

While rs.EOF = False
rs.Delete
rs.MoveNext
Wend

MsgBox "Bill Details Deleted!", vbOKOnly, "Success"

Text1 = ""
Set DataGrid1.DataSource = res
End If
Call closedb

End Sub

Private Sub Form_Load()
On Error Resume Next

If role = "admin" Then
Command1.Visible = False
Command3.Visible = True

Text3.Enabled = False
Text2.Enabled = False
Command2.Enabled = False

End If

End Sub
