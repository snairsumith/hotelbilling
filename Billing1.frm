VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Form12 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "BILLING"
   ClientHeight    =   8070
   ClientLeft      =   2505
   ClientTop       =   2025
   ClientWidth     =   9405
   LinkTopic       =   "Form12"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8070
   ScaleWidth      =   9405
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Height          =   7695
      Left            =   360
      TabIndex        =   9
      Top             =   120
      Width           =   8895
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         Caption         =   "ADD ITEM"
         Height          =   375
         Left            =   6000
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   1860
         Width           =   1095
      End
      Begin VB.TextBox Text3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   4800
         TabIndex        =   7
         Top             =   1920
         Width           =   855
      End
      Begin VB.TextBox Text2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   3600
         TabIndex        =   6
         Top             =   1920
         Width           =   735
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2280
         TabIndex        =   5
         Text            =   "1"
         Top             =   1920
         Width           =   615
      End
      Begin VB.TextBox Text4 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   360
         TabIndex        =   4
         Top             =   1920
         Width           =   1215
      End
      Begin VB.ListBox List1 
         Appearance      =   0  'Flat
         Height          =   615
         ItemData        =   "Billing1.frx":0000
         Left            =   360
         List            =   "Billing1.frx":0002
         Sorted          =   -1  'True
         TabIndex        =   19
         Top             =   2160
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.OptionButton Option2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "AC"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   5640
         TabIndex        =   3
         Top             =   1110
         Width           =   975
      End
      Begin VB.OptionButton Option1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Non AC"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4680
         TabIndex        =   2
         Top             =   1110
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.TextBox Text5 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3480
         TabIndex        =   0
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox lblBillNo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   1080
         Width           =   735
      End
      Begin MSDataGridLib.DataGrid DataGrid1 
         Height          =   3975
         Left            =   360
         TabIndex        =   17
         Top             =   2400
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   7011
         _Version        =   393216
         HeadLines       =   1
         RowHeight       =   15
         AllowDelete     =   -1  'True
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnCount     =   2
         BeginProperty Column00 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   0
            EndProperty
         EndProperty
         BeginProperty Column01 
            DataField       =   ""
            Caption         =   ""
            BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
               Type            =   0
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   2057
               SubFormatType   =   0
            EndProperty
         EndProperty
         SplitCount      =   1
         BeginProperty Split0 
            BeginProperty Column00 
            EndProperty
            BeginProperty Column01 
            EndProperty
         EndProperty
      End
      Begin VB.CommandButton Command3 
         Appearance      =   0  'Flat
         Caption         =   "Edit"
         Height          =   315
         Left            =   1920
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1070
         Width           =   495
      End
      Begin VB.CommandButton Command2 
         Caption         =   "PRINT"
         Height          =   615
         Left            =   6120
         TabIndex        =   10
         Top             =   6840
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   15
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   360
         TabIndex        =   24
         Top             =   6600
         Width           =   1455
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Amount "
         Height          =   255
         Left            =   4920
         TabIndex        =   23
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Unit Price"
         Height          =   255
         Left            =   3600
         TabIndex        =   22
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Quantity "
         Height          =   255
         Left            =   2280
         TabIndex        =   21
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Item"
         Height          =   255
         Left            =   360
         TabIndex        =   20
         Top             =   1680
         Width           =   375
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Waiter ID :"
         Height          =   255
         Left            =   2640
         TabIndex        =   18
         Top             =   1120
         Width           =   855
      End
      Begin VB.Label lblTotal 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   555
         Left            =   4200
         TabIndex        =   15
         Top             =   6480
         Width           =   1080
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "12-12-2018"
         Height          =   255
         Left            =   7920
         TabIndex        =   14
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Date :"
         Height          =   255
         Left            =   7080
         TabIndex        =   13
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Bill No :"
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   1120
         Width           =   615
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "BILLING"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   2520
         TabIndex        =   11
         Top             =   120
         Width           =   2055
      End
   End
End
Attribute VB_Name = "Form12"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim today, u, q, a, itm As String
Dim billno
Dim entrcnt As Integer
Dim roomType As Integer


Sub SelectAllText(tb As TextBox)

tb.SelStart = 0
tb.SelLength = Len(tb.Text)

End Sub




Private Sub Command1_KeyPress(KeyAscii As Integer)
Call prform
End Sub

Private Sub Command3_Click()



Call bindGrid

End Sub

Private Sub DataGrid1_Click()
 Dim i As Long
  
  With DataGrid1
      If .AllowDelete = True Then
          'Debug.Print .Row
          
          If .Row >= 0 Then
              ' Confirm Delete
              If MsgBox("Do you really want to Delete this Record?", vbQuestion + vbYesNo + vbDefaultButton2, "Delete record") = vbYes Then
                  ' Delete current row
                  
                  'NOTE: for .RowBookmark current Row must be visible
                  Call DeleteRow(.RowBookmark(.Row))

              End If
          
          Else
              MsgBox "You must select a row to Delete!", vbExclamation + vbOKOnly, "Delete record"
          End If
          
          .Refresh

      End If
  End With
End Sub

Private Function DeleteRow( _
                      vBookmark As Variant _
                      ) As Boolean
    
  On Error GoTo Err_Handler
  
  Dim oRs As ADODB.Recordset
  
  Set oRs = DataGrid1.DataSource
  If Not oRs Is Nothing Then
      With oRs
          If .State = adStateOpen Then
              .Bookmark = vBookmark
              If Not .EOF Then
              
                  ' Delete the record.
                  .Delete
                .MoveNext
                  
                  
                  If .EOF Then
                      .MoveLast
                  End If

              End If
          End If
      End With
  End If
  
Exit_Function:
  ' clean up
  Set oRs = Nothing
  Exit Function

Err_Handler:
  Debug.Print "ERROR (DeleteRow): " & Err.Description
  MsgBox Err.Description, vbExclamation + vbOKOnly, "Delete record"
  oRs.CancelBatch
  ' Retrieve the current data by requerying the recordset
  oRs.Requery
  Resume Exit_Function
 
End Function

Private Sub Form_Load()
On Error Resume Next
SelectAllText Text1
today = DateTime.Date
Label9.Caption = today
bill_date = today

Call getId
roomType = 1
roomAcOrNon = "Non AC"
Text5.SetFocus


'List1.Visible = False


End Sub


Private Sub Command1_Click()
On Error Resume Next

If Text1.Text = "" Or Text2.Text = "" Or Text3.Text = "" Then
MsgBox "Please Fill All Fields!", vbExclamation, "Error"
Else

itm = Text4.Text
q = "select item from items_tbl where ID=" + itm
Call Getconnection
rs.Open q, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
itm = rs.Fields("item")
End If
Call closedb

Call Getconnection
rs.Open "select * from bill_tbl", cn, adOpenDynamic, adLockPessimistic
rs.AddNew
rs.Fields("waiter") = Text5.Text
rs.Fields("item") = itm
rs.Fields("qty") = Text1.Text
rs.Fields("price") = Text2.Text
rs.Fields("total") = Text3.Text
rs.Fields("billid") = Val(lblBillNo.Text)
rs.Fields("today") = Label9.Caption
rs.Update
Call closedb

Call Getconnection
rs.Open "select sum(total) as total from bill_tbl where billid=" + lblBillNo.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
Text4.Text = 0
Else
lblTotal.Caption = rs.Fields("total")
rs.Close
End If

Call bindGrid
Text1.Text = 1
Text2.Text = "0"
Text3.Text = "0"
Text4.Text = ""

Text4.SetFocus

End If


End Sub

Private Sub Command2_Click()
On Error Resume Next

Call prform

End Sub

Private Sub Frame1_Click()
List1.Visible = False
End Sub

Private Sub Frame2_Click()
List1.Visible = False
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then

Text4.Text = List1.Text
List1.Visible = False
Text1.SetFocus

Call getPrice

End If

End Sub

Private Sub Option1_Click()
roomType = 1
roomAcOrNon = "Non AC"

End Sub

Private Sub Option2_Click()
roomType = 2
roomAcOrNon = "AC"
End Sub

Private Sub Text1_Change()
On Error Resume Next

Text3.Text = ""
If Text4.Text = "" Then
'MsgBox "Fill all Fields!", vbOKOnly, "Error"
Text4.SetFocus

Else
If Text1.Text = "" Then
Else
Text3.Text = Text1.Text * Text2.Text
End If
End If

End Sub

Function getId()
On Error Resume Next

Call Getconnection
rs.Open "select * from bill_tbl", cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
Call closedb
rs.Open "select max(billid) as maxx from bill_tbl", cn, adOpenDynamic, adLockPessimistic
billno = rs.Fields("maxx")
billno = billno + 1
Else
billno = 0
Call closedb
End If

lblBillNo.Text = billno

End Function


Function bindGrid()
On Error Resume Next

If con.State = 1 Then
con.Close
End If

Call Connect
res.CursorLocation = adUseClient
'res.Open "select * from bill_qry where billid=" + lblBillNo.Caption, con, adOpenDynamic, adLockPessimistic
res.Open "select item as Item,qty as Quantity,price as Price,total as Total,ID from bill_tbl  where billid=" + lblBillNo.Text, con, adOpenDynamic, adLockPessimistic
Set DataGrid1.DataSource = res

End Function

Function CalculateTotal()
On Error Resume Next
If con.State = 1 Then
con.Close
End If
Call Getconnection
rs.Open "select sum(total) as total from bill_tbl where billid=" + lblBillNo.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
Text4.Text = 0
Else
lblTotal.Caption = rs.Fields("total")
rs.Close
End If
End Function

'Autocomplete n33raj

Private Sub List1_Click()
List1.Visible = True
End Sub

Private Sub List1_DblClick()
Text4.Text = List1.Text
List1.Visible = False
Text1.SetFocus
Call getPrice
End Sub

Private Sub List1_LostFocus()
List1.Visible = False
End Sub

Private Sub Text1_Click()
List1.Visible = False
End Sub

Private Sub Text1_GotFocus()
On Error Resume Next
Text1.SelStart = 0
Text1.SelLength = Len(Text1.Text)

If Text1.Text = "" Then
Else
Text3.Text = Text1.Text * Text2.Text
End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
Command1_Click
End If

If KeyAscii = 43 Then
Call prform
KeyAscii = 0
clrForm
End If

End Sub

Private Sub Text4_Change()
On Error Resume Next
u = Text4.Text

If Len(u) > 0 Then
If roomType = 1 Then
q = "select ID,item from items_tbl where ID like '" + u + "%' and  itemType='NON AC'"
ElseIf roomType = 2 Then
q = "select ID,item from items_tbl where ID like '" + u + "%' and  itemType='AC'"
End If
Call Getconnection
rs.Open q, cn, adOpenDynamic, adLockPessimistic

List1.Clear
While rs.EOF = False
List1.AddItem (rs.Fields("item"))
rs.MoveNext
Wend

If List1.ListCount > 0 Then
List1.Visible = True
List1.Selected(0) = True
Else
List1.Visible = False
MsgBox "No Item Found!", vbOKOnly, "Error"
Text4.Text = ""
Text4.SetFocus
End If
Call closedb

Else
List1.Visible = False
End If

End Sub

Private Sub Text4_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
Text1.SetFocus
End If

If KeyAscii = 43 Then
Call prform
KeyAscii = 0
clrForm
End If

End Sub

Private Sub Text4_LostFocus()
Call getPrice
End Sub

Function getPrice()
On Error Resume Next

Call Getconnection
rs.Open "select * from items_tbl where ID=" + Text4.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
Text2.Text = rs.Fields("price")
End If
Call closedb

End Function

Private Sub Text5_KeyPress(KeyAscii As Integer)
If (KeyAscii = 13) Then
Text4.SetFocus
End If

If KeyAscii = 43 Then
Call prform
KeyAscii = 0
clrForm
End If

End Sub
Private Sub Text5_LostFocus()
On Error Resume Next

If Text5.Text <> "" Then
Call Getconnection
rs.Open "select ID from login_tbl where role='waiter' and ID=" + Text5.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
MsgBox "Invalid Waiter ID!"
Text5.Text = ""
Text5.SetFocus
End If
Call closedb
End If

End Sub

Public Sub prform()
On Error Resume Next

Call closedb
Call closer

'Unload DataEnvironment1
'Unload DataReport1
total = Val(lblTotal.Caption)
bill = lblBillNo.Text


'If DataEnvironment1.rsCommand2.State = 1 Then
'DataEnvironment1.rsCommand2.Close
'End If

'Load DataEnvironment1
'With DataEnvironment1
'.Command2 (Val(lblBillNo.Caption))
'End With

'DataReport1.Refresh
'DataReport1.Show
Form15.Show


'DataReport1.Visible = False
'DataReport1.PrintReport False, rptRangeAllPages, 1, 1


clrForm


End Sub


Public Sub clrForm()
getId
Text1.Text = ""
Text2.Text = "0"
Text3.Text = "0"
Text4.Text = ""
lblTotal.Caption = "0"
Text4.SetFocus

End Sub

Public Sub clrForm1()
Text1.Text = ""
Text2.Text = "0"
Text3.Text = "0"
Text4.Text = ""
lblTotal.Caption = "0"
Text4.SetFocus
End Sub
