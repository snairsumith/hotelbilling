VERSION 5.00
Begin VB.Form Form2 
   Caption         =   "Change Password"
   ClientHeight    =   5085
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   7770
   LinkTopic       =   "Form2"
   ScaleHeight     =   5085
   ScaleWidth      =   7770
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   5175
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7815
      Begin VB.TextBox Text1 
         Height          =   405
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   1200
         Width           =   4455
      End
      Begin VB.CommandButton Command1 
         Caption         =   "CHANGE PASSWORD"
         Height          =   735
         Left            =   360
         TabIndex        =   8
         Top             =   4200
         Width           =   6735
      End
      Begin VB.TextBox Text4 
         Height          =   375
         IMEMode         =   3  'DISABLE
         Left            =   2520
         PasswordChar    =   "*"
         TabIndex        =   6
         Top             =   3360
         Width           =   4455
      End
      Begin VB.TextBox Text3 
         Height          =   375
         IMEMode         =   3  'DISABLE
         Left            =   2520
         PasswordChar    =   "*"
         TabIndex        =   5
         Top             =   2640
         Width           =   4455
      End
      Begin VB.TextBox Text2 
         Height          =   375
         IMEMode         =   3  'DISABLE
         Left            =   2520
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   1920
         Width           =   4455
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Username :"
         Height          =   255
         Left            =   360
         TabIndex        =   9
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Retype Password :"
         Height          =   255
         Left            =   360
         TabIndex        =   7
         Top             =   3480
         Width           =   1455
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "New Password :"
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   2760
         Width           =   1455
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Old Password :"
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   2040
         Width           =   1455
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "CHANGE PASSWORD"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1680
         TabIndex        =   1
         Top             =   360
         Width           =   4455
      End
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
On Error Resume Next

If Text1.Text = "" Or Text2.Text = "" Or Text3.Text = "" Or Text4.Text = "" Then
MsgBox "Please Fill All Fields!", vbExclamation, "Error"

Else

Call Getconnection
rs.Open "select * from login_tbl where ID=" + Text1.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
If Text2.Text = rs.Fields("password") Then

If Text3.Text = Text4.Text Then
rs.Fields("password") = Text2.Text
rs.Update
Call closedb
MsgBox "Password Changed!", vbInformation, "Success"
Else
MsgBox "Invalid password!", vbExclamation, "Error"
End If
Else
MsgBox "Invalid password!", vbExclamation, "Error"
End If
Else
MsgBox "Invalid Username!", vbExclamation, "Error"
End If


End If

End Sub

