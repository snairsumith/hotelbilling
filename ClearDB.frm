VERSION 5.00
Begin VB.Form Form5 
   Caption         =   "Welcome Bugs"
   ClientHeight    =   1680
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   5040
   LinkTopic       =   "Form5"
   ScaleHeight     =   1680
   ScaleWidth      =   5040
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command4 
      Caption         =   "EXIT"
      Height          =   855
      Left            =   2520
      TabIndex        =   3
      Top             =   840
      Width           =   2535
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Delete All Bills"
      Height          =   855
      Left            =   2520
      TabIndex        =   2
      Top             =   0
      Width           =   2535
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Delete All Items"
      Height          =   855
      Left            =   0
      TabIndex        =   1
      Top             =   840
      Width           =   2535
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Delete All Waiters"
      Height          =   855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2535
   End
End
Attribute VB_Name = "Form5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
On Error Resume Next

Call Getconnection
rs.Open "select * from login_tbl where role='waiter'", cn, adOpenKeyset, adLockPessimistic
While rs.EOF = False
rs.Delete
rs.MoveNext
Wend
MsgBox "Done!"
Call closedb

End Sub

Private Sub Command2_Click()
On Error Resume Next

Call Getconnection
rs.Open "select * from items_tbl", cn, adOpenKeyset, adLockPessimistic
While rs.EOF = False
rs.Delete
rs.MoveNext
Wend
MsgBox "Done!"
Call closedb

End Sub

Private Sub Command3_Click()
On Error Resume Next
Call Getconnection
rs.Open "select * from bill_tbl", cn, adOpenKeyset, adLockPessimistic
While rs.EOF = False
rs.Delete
rs.MoveNext
Wend
MsgBox "Done!"
Call closedb

End Sub

Private Sub Command4_Click()
Unload Me
End Sub
