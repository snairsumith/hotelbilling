VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Form6 
   BackColor       =   &H00FFFFFF&
   Caption         =   "ADD ITEM"
   ClientHeight    =   4830
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   12525
   LinkTopic       =   "Form7"
   ScaleHeight     =   4830
   ScaleWidth      =   12525
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "AddStock.frx":0000
      Left            =   1560
      List            =   "AddStock.frx":000A
      TabIndex        =   2
      Text            =   "Select Type"
      Top             =   1920
      Width           =   4335
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      Top             =   1200
      Width           =   4335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "ADD"
      Height          =   735
      Left            =   4200
      TabIndex        =   5
      Top             =   3840
      Width           =   1695
   End
   Begin VB.TextBox Text1 
      Height          =   405
      Left            =   1560
      TabIndex        =   3
      Top             =   2520
      Width           =   4335
   End
   Begin VB.TextBox Text3 
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   3120
      Width           =   4335
   End
   Begin VB.CommandButton Command2 
      Caption         =   "EXIT"
      Height          =   735
      Left            =   1560
      TabIndex        =   0
      Top             =   3840
      Width           =   1695
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Height          =   3375
      Left            =   6840
      TabIndex        =   10
      Top             =   960
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   5953
      _Version        =   393216
      BackColor       =   16777215
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label5 
      BackColor       =   &H00FFFFFF&
      Caption         =   "AC or NON AC :"
      Height          =   255
      Left            =   360
      TabIndex        =   11
      Top             =   2040
      Width           =   1095
   End
   Begin VB.Line Line1 
      X1              =   6480
      X2              =   6480
      Y1              =   120
      Y2              =   4560
   End
   Begin VB.Label Label3 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Item ID :"
      Height          =   255
      Left            =   360
      TabIndex        =   9
      Top             =   1320
      Width           =   735
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ADD ITEM"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1920
      TabIndex        =   8
      Top             =   240
      Width           =   2535
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Item :"
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   2640
      Width           =   855
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Price :"
      Height          =   255
      Left            =   360
      TabIndex        =   6
      Top             =   3240
      Width           =   855
   End
End
Attribute VB_Name = "Form6"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
On Error Resume Next

If Text1.Text = "" Or Text2.Text = "" Or Text3.Text = "" Then
MsgBox "Please Fill All Fields!", vbExclamation, "Error"
Else

'Call Getconnection
'rs.Open "select * from items_tbl where ID=" + Text2.Text, cn, adOpenDynamic, adLockPessimistic
'If rs.EOF = False Then
'MsgBox "Item Already Exists!"
'Else
Call closedb

Call Getconnection
rs.Open "select * from items_tbl", cn, adOpenDynamic, adLockPessimistic
rs.AddNew
rs.Fields("ID") = Text2.Text
rs.Fields("item") = Text1.Text
rs.Fields("price") = Text3.Text
rs.Fields("itemType") = Combo1.Text
rs.Update
Call closedb

MsgBox "Item Added!", vbOKOnly, "Success"

Call bindGrid

Text1.Text = ""
Text2.Text = ""
Text3.Text = ""

End If

'End If

End Sub

Private Sub Command2_Click()

Unload Me

End Sub

Private Sub Form_Load()

Call bindGrid

End Sub

Function bindGrid()
On Error Resume Next

Call closer
Call Connect
con.CursorLocation = adUseClient
res.Open "select * from items_tbl", con, adOpenKeyset, adLockPessimistic
Set DataGrid1.DataSource = res

End Function


