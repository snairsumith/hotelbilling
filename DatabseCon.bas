Attribute VB_Name = "Module1"
Public cn, rs
Public bill, waiter, role, total, roomAcOrNon, bill_date



Global con As New Connection
Global res As New Recordset

Function Getconnection()

Set cn = New ADODB.Connection
Set rs = New ADODB.Recordset
cn.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + App.Path + "\admin.mdb;Persist Security Info=False"

End Function

Function Connect()
On Error Resume Next

If con.State = 0 Then
con.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\admin.mdb;Persist Security Info=False"
End If

End Function

Function closer()
On Error Resume Next

If res.State = 1 Then
res.Close
End If
End Function

Function closedb()
If rs.State = 1 Then
rs.Close
End If
End Function


Function ArrangeMenus(ByVal role As String)

If role = "super" Then

MDIForm1.m_menu_db.Visible = True
MDIForm1.m_menuAdmin.Visible = True
MDIForm1.m_menuBilling.Visible = True
MDIForm1.m_menuEmp.Visible = True
MDIForm1.m_menuRep.Visible = True
MDIForm1.m_menuStock.Visible = True
MDIForm1.m_menuUser.Visible = True
MDIForm1.m_menuExit.Caption = "Logout"
MDIForm1.menuNewBill.Visible = True



ElseIf role = "admin" Then

MDIForm1.m_menu_db.Visible = False
MDIForm1.m_menuAdmin.Visible = False
MDIForm1.m_menuBilling.Visible = True
MDIForm1.m_menuEmp.Visible = True
MDIForm1.m_menuRep.Visible = True
MDIForm1.m_menuStock.Visible = True
MDIForm1.m_menuUser.Visible = True
MDIForm1.m_menuExit.Caption = "Logout"
MDIForm1.menuNewBill.Visible = False

ElseIf role = "waiter" Then

MDIForm1.m_menu_db.Visible = False
MDIForm1.m_menuAdmin.Visible = True
MDIForm1.m_menuBilling.Visible = True
MDIForm1.m_menuEmp.Visible = False
MDIForm1.m_menuRep.Visible = False
MDIForm1.m_menuStock.Visible = False
MDIForm1.m_menuUser.Visible = True
MDIForm1.m_menuExit.Caption = "Exit"
MDIForm1.menuNewBill.Visible = True

End If

End Function

