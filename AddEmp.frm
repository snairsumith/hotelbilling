VERSION 5.00
Begin VB.Form Form3 
   BackColor       =   &H00FFFFFF&
   Caption         =   "ADD WAITER"
   ClientHeight    =   6120
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8775
   LinkTopic       =   "Form11"
   ScaleHeight     =   6120
   ScaleWidth      =   8775
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Text4 
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2640
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   3840
      Width           =   4815
   End
   Begin VB.TextBox Text3 
      Height          =   405
      IMEMode         =   3  'DISABLE
      Left            =   2640
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   3120
      Width           =   4815
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   2640
      TabIndex        =   2
      Top             =   2400
      Width           =   4815
   End
   Begin VB.TextBox Text1 
      Height          =   405
      Left            =   2640
      TabIndex        =   1
      Top             =   1680
      Width           =   4815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "ADD"
      Height          =   735
      Left            =   5520
      TabIndex        =   5
      Top             =   4800
      Width           =   1935
   End
   Begin VB.CommandButton Command2 
      Caption         =   "EXIT"
      Height          =   735
      Left            =   2640
      TabIndex        =   6
      Top             =   4800
      Width           =   1935
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Confirm Password"
      Height          =   255
      Left            =   600
      TabIndex        =   10
      Top             =   3840
      Width           =   1455
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Password"
      Height          =   255
      Left            =   600
      TabIndex        =   9
      Top             =   3120
      Width           =   1215
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Name :"
      Height          =   255
      Left            =   600
      TabIndex        =   8
      Top             =   2520
      Width           =   855
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Waiter ID :"
      Height          =   255
      Left            =   600
      TabIndex        =   7
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "ADD WAITER"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2760
      TabIndex        =   0
      Top             =   480
      Width           =   3375
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

If Text1.Text = "" Or Text2.Text = "" Or Text3.Text = "" Or Text4.Text = "" Then
MsgBox "Please Fill All Fields!", vbExclamation, "Error"

ElseIf Text3.Text <> Text4.Text Then
MsgBox "Passwords Mismatch!", vbExclamation, "Error"

Else

Call Getconnection
rs.Open "select * from login_tbl", cn, adOpenDynamic, adLockPessimistic
rs.AddNew
rs.Fields("ID") = Text1.Text
rs.Fields("username") = Text2.Text
rs.Fields("password") = Text3.Text
rs.Fields("role") = "waiter"
rs.Update
Call closedb

MsgBox "Employee Details Saved!", vbOKOnly, "Success"

Text1.Text = ""
Text2.Text = ""
Text3.Text = ""
Text4.Text = ""

End If
End Sub

Private Sub Command2_Click()
Unload Me
End Sub
Dim billno As Integer
billno = 0


Function auto_gen()

Call Getconnection
rs.Open "select * from tbl_employee", cn, adOpenDynamic, adLockPessimistic
If rs.EOF = False Then
Call closedb
Call Getconnection
rs.Open "select max(ID) as maxx from tbl_employee", cn, adOpenDynamic, adLockPessimistic
billno = rs.Fields("maxx")
billno = billno + 1
Else
billno = 0
Call closedb
End If
Text1.Text = billno
End Function


Private Sub Form_Load()

'Call auto_gen

End Sub
