VERSION 5.00
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H00E0E0E0&
   Caption         =   "HOME"
   ClientHeight    =   10635
   ClientLeft      =   225
   ClientTop       =   555
   ClientWidth     =   20250
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture1 
      Align           =   3  'Align Left
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   10635
      Left            =   0
      Picture         =   "mdiform1.frx":0000
      ScaleHeight     =   531.75
      ScaleMode       =   2  'Point
      ScaleWidth      =   1012.5
      TabIndex        =   0
      Top             =   0
      Width           =   20250
   End
   Begin VB.Menu m_menuEmp 
      Caption         =   "Waiter"
      Visible         =   0   'False
      Begin VB.Menu menuAddEmp 
         Caption         =   "Add Waiter"
      End
      Begin VB.Menu menuEditEmp 
         Caption         =   "Edit Waiter"
      End
      Begin VB.Menu menuDelEmp 
         Caption         =   "Delete Waiter"
      End
   End
   Begin VB.Menu m_menuStock 
      Caption         =   "Items"
      Visible         =   0   'False
      Begin VB.Menu menuStockAdd 
         Caption         =   "Add Items"
      End
      Begin VB.Menu menuStockEdit 
         Caption         =   "Edit Items"
      End
      Begin VB.Menu menuStockSummary 
         Caption         =   "Item Summary"
      End
   End
   Begin VB.Menu m_menuBilling 
      Caption         =   "Billing"
      Begin VB.Menu menuNewBill 
         Caption         =   "New Bill"
      End
      Begin VB.Menu menuBillInfo 
         Caption         =   "Cancel Bill"
      End
      Begin VB.Menu menuBillCancelItem 
         Caption         =   "Delete Bill Item"
      End
   End
   Begin VB.Menu m_menuUser 
      Caption         =   "Account Settings"
      Begin VB.Menu menuChange 
         Caption         =   "Change Password"
      End
   End
   Begin VB.Menu m_menuAdmin 
      Caption         =   "Admin Login"
   End
   Begin VB.Menu m_menuRep 
      Caption         =   "Reports"
      Visible         =   0   'False
      Begin VB.Menu menuDaily 
         Caption         =   "Daily Report"
      End
      Begin VB.Menu menuWaiterReport 
         Caption         =   "Waiterwise Report"
      End
   End
   Begin VB.Menu m_menu_db 
      Caption         =   "Database"
      Visible         =   0   'False
      Begin VB.Menu menu_dbclear 
         Caption         =   "Clear Tables"
      End
   End
   Begin VB.Menu m_menuExit 
      Caption         =   "Exit"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub m_menuAdmin_Click()
Form1.Show
End Sub

Private Sub m_menuExit_Click()

role = ""
total = ""
bill = ""

If m_menuExit.Caption = "Exit" Then
Unload Me
ElseIf m_menuExit.Caption = "Logout" Then
Call ArrangeMenus("waiter")
End If

End Sub

Private Sub MDIForm_Load()

Call ArrangeMenus("waiter")
'Call ArrangeMenus("admin")

On Error Resume Next
Call Getconnection
rs.Open "select * from bill_tbl where today<=(date()-1)", cn, adOpenKeyset, adLockPessimistic
While rs.EOF = False
rs.Delete
rs.MoveNext
Wend
Call closedb

End Sub

Private Sub menu_clear_db_Click()
Form5.Show
End Sub

Private Sub menu_dbclear_Click()
Form5.Show
End Sub

Private Sub menuAddEmp_Click()
Form3.Show
End Sub


Private Sub menuAdmin_Click()
Form1.Show
End Sub

Private Sub menuBillCancelItem_Click()
Form11.Show
End Sub

Private Sub menuBillInfo_Click()
Form13.Show
End Sub

Private Sub menuChange_Click()
Form2.Show
End Sub

Private Sub menuDaily_Click()
Form9.Show
End Sub

Private Sub menuDelEmp_Click()
Form4.Show
End Sub

Private Sub menuEditEmp_Click()
Form4.Show
End Sub

Private Sub menuNewBill_Click()
Form12.Show
End Sub

Private Sub menuStockAdd_Click()
Form6.Show
End Sub

Private Sub menuStockEdit_Click()
Form7.Show
End Sub

Private Sub menuStockSummary_Click()
Form8.Show
End Sub

Private Sub menuViewEmp_Click()
Form5.Show
End Sub

Private Sub menuWaiterReport_Click()
Form14.Show
End Sub
