VERSION 5.00
Begin VB.Form Form4 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Modify Employee"
   ClientHeight    =   4230
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8790
   LinkTopic       =   "Form10"
   ScaleHeight     =   4230
   ScaleWidth      =   8790
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "UPDATE"
      Height          =   735
      Left            =   5400
      TabIndex        =   3
      Top             =   2760
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   405
      Left            =   2640
      TabIndex        =   2
      Top             =   1920
      Width           =   4815
   End
   Begin VB.CommandButton Command2 
      Caption         =   "DELETE"
      Height          =   735
      Left            =   2640
      TabIndex        =   1
      Top             =   2760
      Width           =   2055
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   2640
      TabIndex        =   0
      Text            =   "- Select -"
      Top             =   1320
      Width           =   4815
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Waiter ID :"
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Name :"
      Height          =   255
      Left            =   480
      TabIndex        =   5
      Top             =   2040
      Width           =   1215
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "EDIT DETAILS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2520
      TabIndex        =   4
      Top             =   240
      Width           =   3735
   End
End
Attribute VB_Name = "Form4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Combo1_Click()
Call Getconnection
rs.Open "select * from login_tbl where ID=" + Combo1.Text + "", cn, adOpenDynamic, adLockPessimistic
Text1.Text = rs.Fields("username")
rs.Close
End Sub

Private Sub Command1_Click()

If Combo1.Text = "- Select -" Then
MsgBox "Select An Employee!", vbOKOnly, "Error"
Else
Call Getconnection
rs.Open "select * from login_tbl where role='waiter' and ID=" + Combo1.Text, cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
MsgBox "No Record Found!", vbOKOnly, "Error"
Else

rs.Fields("username") = Text1.Text
rs.Update
Call closedb

MsgBox "Employee Details Updated!", vbOKOnly, "Success"

Text1.Text = ""
Call bindCombo

End If

End If

End Sub

Private Sub Command2_Click()

Call Getconnection
rs.Open "select * from login_tbl where role='waiter' and ID=" + Combo1.Text + "", cn, adOpenDynamic, adLockPessimistic
If rs.EOF = True Then
MsgBox "No Record Found!", vbOKOnly, "Error"
Else
rs.Delete
MsgBox "Employee Details Deleted!", vbOKOnly, "Success"

Text1.Text = ""
Call bindCombo

End If

End Sub

Function bindCombo()

Combo1.Clear
Combo1.Text = "- Select -"
Call Getconnection
rs.Open "select * from login_tbl where role='waiter'", cn, adOpenDynamic, adLockPessimistic
While rs.EOF = False
Combo1.AddItem (rs.Fields("ID"))
rs.MoveNext
Wend
Call closedb

End Function

Private Sub Form_Load()

Call bindCombo

End Sub
